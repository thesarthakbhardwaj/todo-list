import firebase from 'firebase'
import firestore from 'firebase/firestore'

var firebaseConfig = {
    apiKey: "AIzaSyBS-rlNIYUuewz1sBQoN3oOVOy70pWiq2E",
    authDomain: "geo-locator-5a312.firebaseapp.com",
    databaseURL: "https://geo-locator-5a312.firebaseio.com",
    projectId: "geo-locator-5a312",
    storageBucket: "geo-locator-5a312.appspot.com",
    messagingSenderId: "630144461277",
    appId: "1:630144461277:web:a5a77a792fe53b5c1cae34",
    measurementId: "G-W9MZ094KYP"
  }; 
  // Initialize Firebase
  
  const firebaseApp = firebase.initializeApp(firebaseConfig);
firebaseApp.firestore().settings({ timestampsInSnapshots : true })
  export default firebaseApp.firestore()